FROM nginx:1.15.2-alpine
LABEL maintainer="Curbside Application"

COPY ./build /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx","-g","daemon off;"]

