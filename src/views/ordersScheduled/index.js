import { useEffect, useState, Fragment } from 'react';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';
import Loader from '../../ui-component/Loader';

import axios from 'axios';

import { Grid, Alert, Button } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import MainCard from 'ui-component/cards/MainCard';
import { gridSpacing, qrCode } from 'store/constant';


// ==============================|| TYPOGRAPHY ||============================== //
const OrderDisplay = () => {
    const navigate = useNavigate();
    const [orders, setOrders] = useState([]);
    const [showLoader, setShowLoader] = useState(false);
    const [qrGenerated, setQrGenerated] = useState(false);
    const [qrCodeGenerated, setQrCodeGenerated] = useState('');
    const [showRoute, setShowRoute] = useState(false);
    const [trackingSelected, setTrackingSelected] = useState(false);
    // const dispatch = useDispatch();


  useEffect(() => {
      getData();
  }, []);


    const generateQR = async (item) =>{
        setShowLoader(true);
        const response = await axios.get('http://localhost:8050/v1/web-bff/accounts/byadav/barcode?orderId=' + item.id);
        console.log('qr response:: ', response);
        if(response && response.status === 200) {
            setQrGenerated(true);
            setQrCodeGenerated(response.data);
            setShowRoute(false);
        }
        setShowLoader(false);
        // setTimeout(() => {
        //     setQrGenerated(true);
        //     setQrCodeGenerated(qrCode);
        //     setShowLoader(false);
        // }, 500);
    };

    const getData = async () => {
      const response = await axios.get('http://localhost:8050/v1/web-bff/accounts/byadav/pickup/scheduled');
    //   let scheduledOrders = response.data;
    //   if (scheduledOrders) {
    //       scheduledOrders = scheduledOrders.filter((order) => order.status === "SCHEDULED");
    //   }
    //   console.log('scheduled orders:: ', scheduledOrders);
    //   setOrders(response.data);
      setOrders(response.data);
  };
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
    };
    function success(pos) {
        var crd = pos.coords;
    
        console.log("Your current position is:");
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);
        setShowRoute(true);
    }
    
    function errors(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }
    const trackLoc = () => {
        if (navigator.geolocation) {
            navigator.permissions
              .query({ name: "geolocation" })
              .then(function (result) {
                  console.log(result);
                if (result.state === "granted") {
                    console.log(result.state);
                    //If granted then you can directly call your function here
                    navigator.geolocation.getCurrentPosition(success);
                  } else if (result.state === "prompt") {
                    navigator.geolocation.getCurrentPosition(success, errors, options);
                  } else if (result.state === "denied") {
                    //If denied then you have to show instructions to enable location
                  }
                result.onchange = function () {
                  console.log(result.state);
                };
              });
              setTrackingSelected(true);
          } else {
            alert("Sorry Not available!");
          }
    };

  return (
      <Fragment>
            <h1>{'Your Scheduled Orders'}</h1>
            {(orders != null && orders.length === 0) ? <h4>{'No orders scheduled for pickup.'}</h4> : <MainCard title="">
                <Grid container spacing={gridSpacing}>
                { orders && orders.map( item  => {
                    const { bookingDetails } = item;
                    const storeId = bookingDetails.storeId ? bookingDetails.storeId : '';
                    const bookedDate = bookingDetails.slotDate ? moment(bookingDetails.slotDate).format('DD-MMM-yyyy') : '';
                    const slotStart = bookingDetails.slotStartTime ? moment(bookingDetails.slotStartTime).format('HH:mm') : '';
                    const slotEnd = bookingDetails.slotEndTime ? moment(bookingDetails.slotEndTime).format('HH:mm') : '';
                    const ruthNum = bookingDetails.ruth ? bookingDetails.ruth.replaceAll("[^-\\n](?=.*?-)", "X") : '';
                    const name = bookingDetails.name ? bookingDetails.name : '';
                    return(
                        <Grid key={item.id} item xs={12} sm={10}>
                            <SubCard title={`Order Id : ${ item.id} `}>
                                {showLoader ? <Loader/> : <Grid container direction="column" spacing={1}>
                                    <Grid item>
                                        <MuiTypography variant="overline" display="block" gutterBottom>
                                        Store Id : {storeId}
                                        </MuiTypography>
                                    </Grid>
                                    <Grid item>
                                        <MuiTypography variant="button" display="block" gutterBottom>
                                        Slot Booked Date: {bookedDate} 
                                        </MuiTypography>
                                    </Grid>
                                    <Grid item>
                                        <MuiTypography variant="button" display="block" gutterBottom>
                                        Slot Start Time: {slotStart} 
                                        </MuiTypography>
                                    </Grid>
                                    <Grid item>
                                        <MuiTypography variant="button" display="block" gutterBottom>
                                        Slot End Time: {slotEnd} 
                                        </MuiTypography>
                                    </Grid>
                                    <Grid item>
                                        <MuiTypography variant="button" display="block" gutterBottom>
                                        Name: {name}
                                        </MuiTypography>
                                    </Grid>
                                    <Grid item>
                                        <MuiTypography variant="button" display="block" gutterBottom>
                                        Ruth: {ruthNum}
                                        </MuiTypography>
                                    </Grid>
                                    {qrGenerated && <Grid item>
                                        <Alert severity="info">{`You have checked in successfully. Please show this QR code during pick up.`}</Alert>
                                    </Grid>}
                                    {showRoute && <Grid item>
                                        <h4 style={{ color: 'blue' }}>
                                        {'We have found you the best route to reach the store faster!'}</h4>
                                        <img src={'./Route-in-Google-maps.png'} width="400px" />
                                    </Grid>}
                                    <Grid item>
                                        {qrGenerated ? <img src={`data:image/png;base64,${qrCodeGenerated}`} /> : <Grid container item direction="column" spacing={1}>
                                            <Grid item xs={6}>
                                                <Button
                                                    disableElevation
                                                    disabled={trackingSelected}
                                                    onClick={() => trackLoc(item)}
                                                    size="large"
                                                    type="submit"
                                                    variant="contained"
                                                    color="secondary"
                                                >
                                                {'Start towards store'}
                                                </Button>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Button
                                                    disableElevation
                                                    disabled={qrGenerated}
                                                    onClick={() => generateQR(item)}
                                                    size="large"
                                                    type="submit"
                                                    variant="contained"
                                                    color="secondary"
                                                >
                                                {'Arrived at the store'}
                                                </Button>
                                            </Grid>
                                        </Grid>}
                                    </Grid>
                                </Grid>}
                            </SubCard>
                        </Grid>
                    )
                })}

                </Grid>
            </MainCard>}
    </Fragment>
    
  );
};

export default OrderDisplay;
