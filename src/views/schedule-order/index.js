import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

import axios from "axios";

import { Grid, Button } from "@mui/material";
import MuiTypography from "@mui/material/Typography";

// project imports
import SubCard from "ui-component/cards/SubCard";
import MainCard from "ui-component/cards/MainCard";
import { gridSpacing } from "store/constant";
import { SET_ITEM } from "store/actions";

// ==============================|| TYPOGRAPHY ||============================== //
const OrderDisplay = () => {
  const navigate = useNavigate();
  const [orders, setOrders] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    getData();
  }, []);

  const selectOrderHandler = (item) => {
    navigate("/fill-order-details");
    dispatch({ type: SET_ITEM, item: item });
  };

  const getData = async () => {
    const response = await axios.get(
      "http://localhost:8050/v1/web-bff/accounts/byadav/store/orders"
    );
    setOrders(response.data);
  };
  return (
    <>
      {orders && orders.length ? (
        <MainCard title="Schedule Your Orders ">
          <Grid container spacing={gridSpacing}>
            {orders &&
              orders.length &&
              orders.map((item) => {
                return (
                  <Grid key={item.id} item xs={12} sm={6}>
                    <SubCard title={`Order Id : ${item.id} `}>
                      <Grid container direction="column" spacing={1}>
                        <Grid item>
                          <MuiTypography
                            variant="button"
                            display="block"
                            gutterBottom
                          >
                            Item Count :
                            {item.lineItems && item.lineItems.length}
                          </MuiTypography>
                        </Grid>

                        <Grid item>
                          <MuiTypography
                            variant="button"
                            display="block"
                            gutterBottom
                          >
                            Order Status : {item.status}
                          </MuiTypography>
                        </Grid>
                        <Grid item>
                          <Button
                            disableElevation
                            onClick={() => selectOrderHandler(item)}
                            size="large"
                            type="submit"
                            variant="contained"
                            color="secondary"
                          >
                            Pick your Slot
                          </Button>
                        </Grid>
                      </Grid>
                    </SubCard>
                  </Grid>
                );
              })}
          </Grid>
        </MainCard>
      ) : (
        <MainCard title="No Orders to Schedule">
          <h3> orders </h3>
        </MainCard>
      )}
    </>
  );
};

export default OrderDisplay;
