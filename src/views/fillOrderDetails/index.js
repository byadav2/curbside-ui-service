import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import {
  Grid,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Button,
  OutlinedInput,
} from "@mui/material";
import axios from "axios";
import MuiTypography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";

import { useSelector } from "react-redux";

// project imports
import SubCard from "ui-component/cards/SubCard";
import MainCard from "ui-component/cards/MainCard";
import { gridSpacing } from "store/constant";

// ==============================|| TYPOGRAPHY ||============================== //

const FillOrderDetails = () => {
  const currentItem = useSelector((state) => state.customization.item);
  const { id, storeId, accountId } = currentItem;
  const [lat, setlat] = useState(33.4079);
  const [lng, setLng] = useState(70.5451);
  const theme = useTheme();

  const navigate = useNavigate();
  const item = useSelector((state) => state.customization.item);
  const [storeDetails, setStoreDetails] = useState([]);
  const [date, setDate] = useState();
  const [startTime, setStartTime] = useState();
  const [endTime, setEndTime] = useState();
  const [name, setName] = useState();
  const [ruth, setRuth] = useState();
  const [timeSlot, setTimeSlot] = useState([]);
  const [startEndTime, setStartEndTime] = useState();

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (date) {
      getTimeSlots();
    }
  }, [date]);

  const handleChangeDate = (event) => {
    setDate(event.target.value);
  };

  const handleChangeTime = (e) => {
    setStartEndTime(e.target.value);
    let timeSplit = e.target.value;
    let timearr = timeSplit.split("/");
    setStartTime(timearr[0].trim());
    setEndTime(timearr[1].trim());
  };

  const getData = async () => {
    const response = await axios.post(
      `http://localhost:8050/v1/web-bff/accounts/byadav/picking/initiate?storeId=${storeId}&lat=${lat}6&lng=${lng}`
    );
    setStoreDetails(response.data.storeDetails);
  };

  const getTimeSlots = async () => {
    let updatedTimeSlots = [];
    const response = await axios.post(
      `http://localhost:8050/v1/web-bff/accounts/byadav/picking/initiate?storeId=${storeId}&lat=${lat}6&lng=${lng}&slotDate=${date}`
    );
    updatedTimeSlots =
      response.data.storeDetails[0].allDaysSlotDetailList[0].slotDetailsDTOList.map(
        (item) => {
          return {
            slotStartTime: item.slotStartTime,
            slotEndTime: item.slotEndTime,
          };
        }
      );
    setTimeSlot(updatedTimeSlots);
  };

  const postData = async (payload) => {
    const response = await axios.post(
      "http://localhost:8050/v1/web-bff/accounts/byadav/pickup/schedule",
      payload
    );
    if (response) {
      navigate("/orders-scheduled");
    }
  };
  const ConfirmPickUpHandler = () => {
    const payload = {
      orderId: id,
      storeId: parseInt(storeId, 10),
      slotDate: date.trim(),
      slotStartTime: startTime,
      slotEndTime: endTime,
      customerId: accountId,
      name: name,
      ruth: ruth,
    };
    postData(payload);
  };

  return (
    <MainCard>
      <Grid container spacing={gridSpacing}>
        <Grid item xs={12} sm={6}>
          <SubCard title="Your Order">
            <Grid container direction="column" spacing={1}>
              <Grid item>
                <MuiTypography variant="button" display="block" gutterBottom>
                  Order Id : {item.id}
                </MuiTypography>
              </Grid>
              <Grid item>
                <MuiTypography variant="button" display="block" gutterBottom>
                  Item Count :{item.lineItems && item.lineItems.length}
                </MuiTypography>
              </Grid>
              <Grid item>
                <MuiTypography variant="button" display="block" gutterBottom>
                  Order Status : {item.status}
                </MuiTypography>
              </Grid>
            </Grid>
          </SubCard>
        </Grid>
        {storeDetails && storeDetails.length ? (
          <>
            <Grid item xs={12} sm={6}>
              {storeDetails.map((store) => {
                return (
                  <SubCard
                    title={` Store Id :${store.storeId} At 5.8 KM Away `}
                  >
                    <Grid container direction="column" spacing={1}>
                      <Grid>
                        <InputLabel id="demo-simple-select-label">
                          Available Date Slot
                        </InputLabel>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={date}
                          label="Date"
                          onChange={handleChangeDate}
                        >
                          {store.allDaysSlotDetailList.map((item) => {
                            return (
                              <MenuItem key={item.slotDate} value={`${item.slotDate} `}>
                                {moment(item.slotDate).format("DD-MMM-yyyy")}
                              </MenuItem>
                            );
                          })}
                        </Select>
                        &nbsp; &nbsp;
                      </Grid>

                      <Grid>
                        <InputLabel id="demo-simple-select-label">
                          Available Time Slot{" "}
                        </InputLabel>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-labelTime"
                          id="demo-simple-select"
                          value={startEndTime}
                          label="Time"
                          onChange={handleChangeTime}
                        >
                          {timeSlot &&
                            timeSlot &&
                            timeSlot.map((item) => {
                              return (
                                <MenuItem
                                  key={item.slotStartTime}
                                  value={`${item.slotStartTime} / ${item.slotEndTime}`}
                                >
                                  {moment(item.slotStartTime).format("HH:mm")}
                                  {" - "}{" "}
                                  {moment(item.slotEndTime).format("HH:mm")}
                                </MenuItem>
                              );
                            })}
                        </Select>
                      </Grid>

                      <Grid item>
                        &nbsp; &nbsp;
                        <FormControl
                          fullWidth
                          sx={{ ...theme.typography.customInput }}
                        >
                          <InputLabel>Name</InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-email-register"
                            type="email"
                            size={"small"}
                            value={name}
                            name="email"
                            label="Name"
                            onChange={(e) => setName(e.target.value)}
                            inputProps={{}}
                          />
                        </FormControl>
                        <FormControl
                          fullWidth
                          sx={{ ...theme.typography.customInput }}
                        >
                          <InputLabel htmlFor="outlined-adornment-password-register">
                            Ruth
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-password-register"
                            type={"text"}
                            value={ruth}
                            name="Ruth"
                            label="Ruth"
                            size="small"
                            onChange={(e) => setRuth(e.target.value)}
                            inputProps={{}}
                          />
                        </FormControl>
                        <Button
                          disableElevation
                          fullWidth
                          size="large"
                          type="submit"
                          variant="contained"
                          color="secondary"
                          onClick={ConfirmPickUpHandler}
                        >
                          Proceed
                        </Button>
                      </Grid>
                    </Grid>
                  </SubCard>
                );
              })}
            </Grid>
          </>
        ) : (
          <>
            <Grid item xs={12} sm={6}>
              <SubCard title="Your Order">
                <Grid container direction="column" spacing={1}>
                  <Grid item>
                    <MuiTypography
                      variant="button"
                      display="block"
                      gutterBottom
                    >
                      No Orders Found
                    </MuiTypography>
                  </Grid>
                </Grid>
              </SubCard>
            </Grid>
          </>
        )}
      </Grid>
    </MainCard>
  );
};

export default FillOrderDetails;
