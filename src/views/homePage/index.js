// material-ui
import { Typography } from '@mui/material';

// project imports
import MainCard from 'ui-component/cards/MainCard';

// ==============================|| Home PAGE ||============================== //

const HomePage = () => (
    <MainCard title="Hola, BYADAV">
        <Typography variant="h2">
            Schedule curbside pickup for your orders.
        </Typography>
    </MainCard>
);

export default HomePage;
