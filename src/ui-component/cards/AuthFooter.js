// material-ui
import { Typography, Stack } from '@mui/material';

// ==============================|| FOOTER - AUTHENTICATION 2 & 3 ||============================== //

const AuthFooter = () => (
    <Stack direction="row" justifyContent="space-between">
        <Typography variant="subtitle2" underline="hover">
            Falabella
        </Typography>
        <Typography variant="subtitle2" underline="hover">
            &copy; nerds@work
        </Typography>
    </Stack>
);

export default AuthFooter;
