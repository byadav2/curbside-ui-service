import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useSelector,useDispatch } from 'react-redux';
import { SET_LOGIN } from 'store/actions';



export default function AlertDialog(props) {
    const {modalValue,trigger} = props;
    const dispatch = useDispatch();


    React.useEffect(()=>{
        if(modalValue){
            setOpen(true);
        }
    },[trigger])

  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
    // dispatch({ type: SET_LOGIN, logged: true });

  };

  const handleAgree = () => {
    setOpen(false);
    dispatch({ type: SET_LOGIN, logged: true });

  };

  return (
    <div>
      {/* <Button variant="outlined" onClick={handleClickOpen}>
        Open alert dialog
      </Button> */}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Use Google's location service?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Let Google help apps determine location. This means sending anonymous
            location data to Google, even when no apps are running.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}