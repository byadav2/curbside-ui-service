// assets
import { IconBrandChrome, IconHelp } from '@tabler/icons';

// constant
const icons = { IconBrandChrome, IconHelp };

// ==============================|| SAMPLE PAGE & DOCUMENTATION MENU ITEMS ||============================== //

const pages = {
    id: 'sample-docs-roadmap',
    type: 'group',
    children: [
        {
            id: 'home-page',
            title: 'Home Page',
            type: 'item',
            url: '/home-page',
            icon: icons.IconBrandChrome,
            breadcrumbs: false
        },
        {
            id: 'order-display',
            title: 'Schedule Orders',
            type: 'item',
            url: '/schedule-order',
            icon: icons.IconBrandChrome,
            breadcrumbs: false
        },
        {
            id: 'orders-scheduled',
            title: 'Orders Scheduled',
            type: 'item',
            url: 'orders-scheduled',
            icon: icons.IconBrandChrome,
            breadcrumbs: false
        }
    ]
};

export default pages;
