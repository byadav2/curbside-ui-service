import { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';


// sample page routing
const HomePage = Loadable(lazy(() => import('views/homePage')));
const OrderDisplay = Loadable(lazy(() => import('views/schedule-order')));
const FillOrderDetails = Loadable(lazy(() => import('views/fillOrderDetails')));
const ScheduledOrders = Loadable(lazy(() => import('views/ordersScheduled')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <HomePage />
        },
        {
            path: '/home-page',
            element: <HomePage />
        },
        {
            path: '/schedule-order',
            element: <OrderDisplay />
        },
        {
            path: '/fill-order-details',
            element: <FillOrderDetails />
        },
        {
            path: '/orders-scheduled',
            element: <ScheduledOrders />
        }
       
    ]
};

export default MainRoutes;
