import { useRoutes } from 'react-router-dom';
import { useSelector } from 'react-redux';


// routes
import AuthenticationRoutes from './AuthenticationRoutes';
import config from 'config';
import MainRoutes from './MainRoutes';

// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes() {
    const logged = useSelector((state) => state.customization.logged);
    
    let routeConfig =  AuthenticationRoutes;
    if(logged){
        routeConfig = MainRoutes;
    }
    return useRoutes([routeConfig], config.basename);
}
